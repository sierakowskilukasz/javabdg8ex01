package com.homework.exercise01;

import java.util.List;
// TODO: Add java doc
// TODO: Add missing annotations
@ToString
public class Statistics {
	private Integer words;
	private Integer sentences;
	private Integer vowels;
	private Integer consonants;
	private Integer whiteSigns;
	private Float avgWordLength;
	private Float averageSentenceLength;
	private List<WordStatistic> wordStatistics;

	// TODO: Add java doc
	class WordStatistic {
		private String word;
		private Integer quantity;
	}

	// TODO: Implements getters, setters
}
