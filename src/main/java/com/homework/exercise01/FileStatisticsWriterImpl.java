package com.homework.exercise01;

import java.io.File;
import java.io.FileWriter;

// TODO: Add java doc
// TODO: Add missing annotation
public class FileStatisticsWriterImpl implements FileStatisticsWriter {

	private File file;

	public FileStatisticsWriterImpl(File file) {
		this.file = file;
	}

	public void write(String text){
		try {
			new FileWriter(file).write(text);
		}
		catch(Exception error){
			log.error(error.getMessage(), error);
		}
	}
}
