package com.homework.exercise01;

import java.io.File;

// TODO: Addd javaDoc
public class TextAnalyzer {

	private FileStatisticsWriter fileWriter;

	public TextAnalyzer(FileStatisticsWriter fileWriter) {
		this.fileWriter = fileWriter;
	}

	public Statistics generateStatistics(String text){
		// TODO: Generate, calculate statistics and return result
		Statistics statistics = Statistics.builder()
			.words(countWords(text))
			.sentences(countSentences(text))
			// TODO: Calculate other statistics in the same way
			.build();
	}

	public void generateStatisticsAndSaveToFile(File file){
		// Generate and calculate statistics
		String statistics;
		fileWriter.write(statistics);
	}

	public Integer countWords(String text){
		// TODO: Add implementation
	}

	public Integer countSentences(String text){
		// TODO: Add implementation
	}
	// TODO: Add missing methods
}
